## Description
Assignment of Varadise interview by Isaac Ng

## Application
1. Golang backend
- port 8080
2. PhpLiteAdmin
- port 8888
3. React SPA
- port 8000

## Deployment for Ubuntu

1. Clone the project
```bash
$ git clone https://gitlab.com/xyz-personal/assignment/varadise.git
$ cd varadise
```

2. Run the install script to install Docker and docker-compose
```bash
$ sudo chmod +x install.sh
$ ./install.sh
```

2. Run the deploy script
```bash
$ sudo chmod +x deploy.sh
$ ./deploy.sh
```

## Reference
- [How to Install Docker Compose on Ubuntu 22.04](https://linuxhint.com/install-docker-compose-ubuntu-22-04/)