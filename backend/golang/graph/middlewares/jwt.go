package middlewares

import (
	"context"
	"net/http"
	"strings"
)

func JwtMiddleware() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			tokenStr := TokenFromHTTPRequestgo(r)

			ctx := context.WithValue(r.Context(), "JWT", tokenStr)
			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)
		})
	}
}

func TokenFromHTTPRequestgo(r *http.Request) string {
	reqToken := r.Header.Get("Authorization")
	var tokenStr string

	splitToken := strings.Split(reqToken, "Bearer ")
	if len(splitToken) > 1 {
		tokenStr = splitToken[1]
	}
	return tokenStr
}
