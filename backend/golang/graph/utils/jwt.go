package utils

import (
	"errors"
	"test3/graph/model"
	"time"

	"github.com/golang-jwt/jwt/v4"
)

var jwtKey = []byte("my_secret_key")

var users = map[string]string{
	"admin": "password",
}

type Claims struct {
	Username string `json:"username"`
	jwt.RegisteredClaims
}

type JWT struct {
	Value     string
	ExpiredAt time.Time
}

func CreateJWT(credentials model.Login) (JWT, error) {
	var result = JWT{Value: "", ExpiredAt: time.Now()}
	expectedPassword, ok := users[credentials.Username]

	if !ok || expectedPassword != credentials.Password {
		var err = errors.New("Invalid token")
		return result, err
	}

	expiredAt := time.Now().Add(24 * time.Hour)
	claims := &Claims{
		Username: credentials.Username,
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(expiredAt),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(jwtKey)

	if err != nil {
		return result, err
	}

	result = JWT{Value: tokenString, ExpiredAt: expiredAt}

	return result, nil
}

func ValidateJWT(tokenStr string) (bool, error) {
	claims := &Claims{}

	token, err := jwt.ParseWithClaims(tokenStr, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})

	if err != nil {
		return false, err
	}
	if !token.Valid {
		tokenErr := errors.New("Invalid token")
		return false, tokenErr
	}

	return true, nil
}
