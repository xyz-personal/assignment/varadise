package buildings

import (
	"html"
	"log"
	"test3/graph/model"
	"test3/internal/database"
)

type BuildingRow struct {
	OSEBuildingID                 string   `json:"OSEBuildingID"`
	DataYear                      int      `json:"DataYear"`
	BuildingType                  string   `json:"BuildingType"`
	PrimaryPropertyType           string   `json:"PrimaryPropertyType"`
	PropertyName                  string   `json:"PropertyName"`
	Address                       *string  `json:"Address"`
	City                          *string  `json:"City"`
	State                         *string  `json:"State"`
	ZipCode                       *float64 `json:"ZipCode"`
	TaxParcelIdentificationNumber *string  `json:"TaxParcelIdentificationNumber"`
	CouncilDistrictCode           int      `json:"CouncilDistrictCode"`
	Latitude                      float64  `json:"Latitude"`
	Longitude                     float64  `json:"Longitude"`
	YearBuilt                     *int     `json:"YearBuilt"`
	NumberofBuildings             *int     `json:"NumberofBuildings"`
	NumberofFloors                *int     `json:"NumberofFloors"`
	BuildingGfa                   string   `json:"BuildingGFA"`
	EnergyStarRating              string   `json:"EnergyStarRating"`
	Metrics                       string   `json:"Metrics"`
}

func GetAll(keyword string, page int, pageSize int) ([]BuildingRow, int) {
	var count int
	row := database.Db.QueryRow("SELECT COUNT(*) AS count FROM `buildings`")
	err := row.Scan(&count)
	if err != nil {
		log.Fatal(err)
	}

	/* Prevent SQL injection */
	kw := html.EscapeString(keyword)

	rows, err := database.Db.Query("SELECT`M`.OSEBuildingID,`M`.DataYear,`M`.BuildingType,`M`.PrimaryPropertyType,`M`.PropertyName,`M`.Address,`M`.City,`M`.State,`M`.ZipCode,`M`.TaxParcelIdentificationNumber,`M`.CouncilDistrictCode,`M`.Latitude,`M`.Longitude,`M`.YearBuilt,`M`.NumberofBuildings,`M`.NumberofFloors,JSON_GROUP_ARRAY(DISTINCT JSON_OBJECT('OSEBuildingID',`M`.OSEBuildingID,'PropertyUseType',`M`.PropertyUseType,'PropertyUseTypeGFA',`M`.PropertyUseTypeGFA))FILTER(WHERE`M`.PropertyUseType IS NOT NULL)AS BuildingsGfa,JSON_GROUP_ARRAY(DISTINCT JSON_OBJECT('OSEBuildingID',`M`.OSEBuildingID,'YearsEnergyStarCertified',COALESCE(`M`.YearsENERGYSTARCertified,''),'EnergyStarScore',`M`.ENERGYSTARScore))FILTER(WHERE`M`.ENERGYSTARScore IS NOT NULL)AS EngyStarRating,JSON_GROUP_ARRAY(DISTINCT JSON_OBJECT('OSEBuildingID',`M`.OSEBuildingID,'Metric',`M`.Metric,'Unit',`M`.Unit,'Value',`M`.Value))FILTER(WHERE`M`.Metric IS NOT NULL)AS Metrics FROM(SELECT*FROM`buildings`LEFT JOIN`buildings_gfa`ON`buildings`.OSEBuildingID=`buildings_gfa`.OSEBuildingID LEFT JOIN`energy_star_rating`ON`buildings`.OSEBuildingID=`energy_star_rating`.OSEBuildingID LEFT JOIN`metrics`ON`buildings`.OSEBuildingID=`metrics`.OSEBuildingID)AS M WHERE`M`.PropertyName LIKE ? GROUP BY`M`.OSEBuildingID LIMIT ? OFFSET ?", "%"+kw+"%", pageSize, (page-1)*pageSize)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var buildings []BuildingRow
	for rows.Next() {
		var building BuildingRow

		err = rows.Scan(
			&building.OSEBuildingID,
			&building.DataYear,
			&building.BuildingType,
			&building.PrimaryPropertyType,
			&building.PropertyName,
			&building.Address,
			&building.City,
			&building.State,
			&building.ZipCode,
			&building.TaxParcelIdentificationNumber,
			&building.CouncilDistrictCode,
			&building.Latitude,
			&building.Longitude,
			&building.YearBuilt,
			&building.NumberofBuildings,
			&building.NumberofFloors,
			&building.BuildingGfa,
			&building.EnergyStarRating,
			&building.Metrics,
		)
		if err != nil {
			log.Fatal(err)
		}

		buildings = append(buildings, building)
	}

	if err = rows.Err(); err != nil {
		log.Fatal(err)
	}
	return buildings, count
}

func GetAvgEUI() []model.AvgEui {
	rows, err := database.Db.Query("SELECT PrimaryPropertyType,avg(eui)AS`average_eui`FROM(SELECT`t`.`OSEBuildingID`AS`id`,`t`.`PrimaryPropertyType`,`t2`.`electricity`/`t1`.`gfa`AS`eui`FROM`buildings`t LEFT JOIN(SELECT`OSEBuildingID`AS`id`,SUM(`PropertyUseTypeGFA`)AS`gfa`FROM`buildings_gfa`GROUP BY`OSEBuildingID`)t1 ON`t`.`OSEBuildingID`=`t1`.`id`LEFT JOIN(SELECT`OSEBuildingID`AS id,value AS`electricity`FROM metrics WHERE metric='Electricity')t2 ON`t`.`OSEBuildingID`=`t2`.`id`)GROUP BY`PrimaryPropertyType`")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var buildings []model.AvgEui
	for rows.Next() {
		var building model.AvgEui

		err = rows.Scan(
			&building.PrimaryPropertyType,
			&building.Eui,
		)
		if err != nil {
			log.Fatal(err)
		}

		buildings = append(buildings, building)
	}

	if err = rows.Err(); err != nil {
		log.Fatal(err)
	}
	return buildings
}
