package database

import (
	"database/sql"
	"log"

	_ "github.com/mattn/go-sqlite3"
)

var Db *sql.DB

func InitDB() {
	db, err := sql.Open("sqlite3", "./internal/database/data.db")
	if err != nil {
		log.Fatal(err)
	}

	if err = db.Ping(); err != nil {
		log.Fatal(err)
	}

	Db = db
}

func CloseDB() error {
	return Db.Close()
}
