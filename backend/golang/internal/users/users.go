package users

import (
	"test3/graph/model"
	"test3/graph/utils"

	"github.com/golang-jwt/jwt/v4"
)

var jwtKey = []byte("my_secret_key")

var users = map[string]string{
	"admin": "password",
}

type Claims struct {
	Username string `json:"username"`
	jwt.RegisteredClaims
}

func Login(credentials model.Login) (utils.JWT, error) {
	result, err := utils.CreateJWT(credentials)
	return result, err
}
