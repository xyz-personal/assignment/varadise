import { CodegenConfig } from '@graphql-codegen/cli'
 
const config: CodegenConfig = {
  schema: process.env.REACT_APP_API_BASE_URL,
  documents: './src/**/*.graphql',
  generates: {
    './src/gql/generated/graphql.ts': {
      plugins: ['add', 'typescript', 'typescript-operations', 'typescript-react-query'],
      config: {
        content: ['/* eslint-disable */', '// @ts-ignore'],
        fetcher: '../fetcher#fetchData',
        exposeQueryKeys: true,
        addInfiniteQuery: false,
        exposeFetcher: true,
        pureMagicComment: true,
      }
    }
  }
}
 
export default config