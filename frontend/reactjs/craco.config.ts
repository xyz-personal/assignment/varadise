/* eslint-disable */
// @ts-ignore
import * as cracoAlias from 'craco-alias';
import { inject } from './linaria';

export const webpack = {
	configure(config: any) {
		for (const { oneOf } of config.module.rules) if (oneOf && oneOf.some(inject)) break;
		return config;
	},
};

export const plugins = [{
	plugin: cracoAlias,
	options: {
		baseUrl: ".",
		source: "tsconfig",
		tsConfigPath: "./tsconfig.json"
	}
}]