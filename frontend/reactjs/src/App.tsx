import React, { useState } from 'react';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import 'normalize.css';

import { AuthProvider } from '@contexts/auth';
import Layout from '@components/Layout';

const withApplication = (Component: React.FC<any>) => (props: any) => {
	const [queryClient] = useState(
		() =>
			new QueryClient({
				defaultOptions: {
					queries: {
						staleTime: 300 * 1000,
					},
					mutations: {
						cacheTime: 5000,
					},
				},
			})
	);

	return (
		<QueryClientProvider client={queryClient}>
			<AuthProvider>
				<Layout>
					<Component {...props} />
				</Layout>
			</AuthProvider>
		</QueryClientProvider>
	);
};

export default withApplication;
