import { styled } from '@linaria/react';
import background from '@public/bg.gif';

interface Props {
  children?: React.ReactElement
}

const Container = styled.main`
  height: 100vh;
  width: 100vw;
  display: flex;
  align-items: center;
  justify-content: center;
  background-image: url(${background});
  background-size: contain;
  background-position: bottom center;
  background-repeat: no-repeat;
`;

const Layout = ({ children }: Props) => (
  <Container>
    {children}
  </Container>
)

export default Layout