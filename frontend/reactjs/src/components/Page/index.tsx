import { styled } from '@linaria/react';
import { useNavigate, useLocation } from 'react-router-dom';
import { Card, Menu } from 'antd';

import useAuth from '@contexts/auth';

interface Props {
	children?: React.ReactElement;
}

const Wrapper = styled(Card)`
	height: 60vh;
	width: 80vw;
	background-color: #ffffffd6;
`;

const Page = ({ children }: Props) => {
	const navigate = useNavigate();
  const { pathname } = useLocation();
	const [, { logout }] = useAuth();

	const onClick = (e: any) => {
    console.log(e.key)
		switch (e.key) {
			case '/':
			case '/chart':
				navigate(e.key);
				break;
			case '/login':
				logout();
				navigate(e.key);
				break;
		}
	};

  const items = [
    {
      label: 'Overview',
      key: '/',
    },
    {
      label: 'Chart',
      key: '/chart',
    },
    {
      label: 'Logout',
      key: '/login',
    },
  ]

	return (
		<Wrapper
			title={
				<Menu
					onClick={onClick}
					mode="horizontal"
          selectedKeys={items.filter(({ key }) => pathname === key).map(({ key }) => key)}
					items={items}
				/>
			}
		>
			{children}
		</Wrapper>
	);
};

export default Page;
