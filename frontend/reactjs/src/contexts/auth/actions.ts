import { Dispatch } from '@contexts/types';
import { RequestLoginMutation } from '@gql/generated/graphql';
import { AuthAction, LOGON, LOGOUT, INIT_TOKEN } from './types';

export const logon = (dispatch: Dispatch<AuthAction>) => (data: RequestLoginMutation['login']) => {
  localStorage.setItem('jwtToken', data.value);
  dispatch({ type: LOGON, payload: { mounted: true, ...data } });
};

export const initToken = (dispatch: Dispatch<AuthAction>) => () => {
  dispatch({ type: INIT_TOKEN, payload: { mounted: true, jwtToken: localStorage.jwtToken } });
};

export const logout = (dispatch: Dispatch<AuthAction>) => () => {
  localStorage.removeItem('jwtToken');
  dispatch({ type: LOGOUT, payload: { mounted: true, jwtToken: '' } });
};
