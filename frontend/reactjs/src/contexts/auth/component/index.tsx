import React, { Fragment, useState, useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import jwtDecode from 'jwt-decode';

import useAuth from '../index';

interface Props {
  children: React.ReactNode;
}

const AuthComponent: React.FC<Props> = ({ children }) => {
  const navigate = useNavigate();
  const { pathname } = useLocation();

  const [loading, setLoading] = useState(true);
  const [{ mounted, jwtToken }, { initToken, logout }] = useAuth();

  useEffect(() => {
    const init = async () => {
      if (localStorage.jwtToken) {
        try {
          const data = jwtDecode(localStorage.jwtToken);
          const { exp } = data as any;
          const now = Math.floor((new Date()).getTime() / 1000);
          const diff = exp - now;
          setTimeout(logout, diff * 1000);
          initToken();
        } catch (e: any) {
          console.log('[Get Applicant Error]', e);
          logout();
        }
      } else {
        logout();
      }
    };

    if (!mounted) init();
  }, [mounted]);

  useEffect(() => {
    if (mounted) {
      if (jwtToken && pathname.startsWith('/login')) {
        navigate('/', { replace: true });
      } else if (!jwtToken && !pathname.startsWith('/login')) {
        navigate('/login', { replace: true });
      }
      setLoading(false);
    }
  }, [mounted, pathname]);

  if (!mounted || loading) return null;

  return <Fragment>{children}</Fragment>;
};

export default AuthComponent;
