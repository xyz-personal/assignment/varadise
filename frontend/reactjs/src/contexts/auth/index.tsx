import { createContext, useReducer, useContext } from 'react';
import { Dispatch } from '@contexts/types';

import Component from './component';
import { initToken, logon, logout } from './actions';
import { AuthState, AuthAction } from './types';
import authReducer, { initState } from './reducer';

const AuthContext = createContext<[AuthState, Dispatch<AuthAction>]>([initState, () => {}]);

export const AuthProvider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  const value = useReducer(authReducer, initState);

  return (
    <AuthContext.Provider value={value}>
      <Component>{children}</Component>
    </AuthContext.Provider>
  );
};

export const useAuth = (): [AuthState, Record<string, any>] => {
  const context = useContext(AuthContext);
  if (context === undefined) {
    throw new Error('[Auth] useAuth must be used within AuthProvider.');
  }
  const [state, dispatch] = context;

  const dispatchActions = {
    initToken: initToken(dispatch),
    logon: logon(dispatch),
    logout: logout(dispatch),
  };

  return [state, dispatchActions];
};

export default useAuth;
