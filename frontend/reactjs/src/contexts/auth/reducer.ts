import { AuthState, AuthAction, LOGON, LOGOUT, INIT_TOKEN } from './types';

export const initState: AuthState = {
  mounted: false,
  jwtToken: '',
};

export const authReducer = (state = initState, action: AuthAction) => {
  const { type, payload } = action;
  switch (type) {
    case LOGON:
    case LOGOUT:
    case INIT_TOKEN:
      return { ...state, ...payload };
    default:
      return state;
  }
};

export default authReducer;
