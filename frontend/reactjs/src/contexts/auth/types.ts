export const LOGON = 'LOGON';
export const LOGOUT = 'LOGOUT';
export const INIT_TOKEN = 'INIT_TOKEN';

export interface AuthState {
  mounted: boolean;
  jwtToken: string;
}

export interface AuthAction {
  type: string;
  payload: Partial<AuthState>;
}
