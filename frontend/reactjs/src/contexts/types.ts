export type Dispatch<T> = (action: T) => void;
