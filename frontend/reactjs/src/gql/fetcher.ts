import { request, Variables } from 'graphql-request';

export const fetchData = <TData, TVariables extends Variables>(
	query: string,
	variables?: TVariables,
	headers?: RequestInit['headers']
): (() => Promise<TData>) => {
	return async () => {
		try {
			const requestHeaders = {
				...headers,
				...(localStorage.jwtToken && { Authorization: `Bearer ${localStorage.jwtToken}` }),
			};

			const data = await request<TData, TVariables>({
				url: process.env.REACT_APP_API_BASE_URL ?? '',
				document: query,
				variables,
				requestHeaders,
			} as any);

			return data;
		} catch (e: any) {
			if (e.response.errors?.[0]?.message) {
				throw e.response.errors[0].message;
			}
			throw e.message;
		}
	};
};
