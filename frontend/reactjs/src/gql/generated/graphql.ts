/* eslint-disable */
// @ts-ignore
import { useQuery, useMutation, UseQueryOptions, UseMutationOptions } from '@tanstack/react-query';
import { fetchData } from '../fetcher';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Timestamp: any;
};

export type AvgEui = {
  __typename?: 'AvgEUI';
  EUI: Scalars['Float'];
  PrimaryPropertyType: Scalars['String'];
};

export type Building = {
  __typename?: 'Building';
  Address?: Maybe<Scalars['String']>;
  BuildingGFA: Array<BuildingGfa>;
  BuildingType: Scalars['String'];
  City?: Maybe<Scalars['String']>;
  CouncilDistrictCode: Scalars['Int'];
  DataYear: Scalars['Int'];
  EnergyStarRating: Array<EnergyStarRating>;
  Latitude: Scalars['Float'];
  Longitude: Scalars['Float'];
  Metrics: Array<Metrics>;
  NumberofBuildings?: Maybe<Scalars['Int']>;
  NumberofFloors?: Maybe<Scalars['Int']>;
  OSEBuildingID: Scalars['ID'];
  PrimaryPropertyType: Scalars['String'];
  PropertyName: Scalars['String'];
  State?: Maybe<Scalars['String']>;
  TaxParcelIdentificationNumber?: Maybe<Scalars['String']>;
  YearBuilt?: Maybe<Scalars['Int']>;
  ZipCode?: Maybe<Scalars['Float']>;
};

export type BuildingGfa = {
  __typename?: 'BuildingGFA';
  OSEBuildingID: Scalars['Int'];
  PropertyUseType: Scalars['String'];
  PropertyUseTypeGFA: Scalars['Float'];
};

export type Buildings = {
  __typename?: 'Buildings';
  items?: Maybe<Array<Building>>;
  page: Scalars['Int'];
  size: Scalars['Int'];
  total: Scalars['Int'];
};

export type EnergyStarRating = {
  __typename?: 'EnergyStarRating';
  EnergyStarScore: Scalars['Float'];
  OSEBuildingID: Scalars['Int'];
  YearsEnergyStarCertified: Scalars['String'];
};

export type Login = {
  password: Scalars['String'];
  username: Scalars['String'];
};

export type Metrics = {
  __typename?: 'Metrics';
  Metric: Scalars['String'];
  OSEBuildingID: Scalars['Int'];
  Unit: Scalars['String'];
  Value: Scalars['Float'];
};

export type Mutation = {
  __typename?: 'Mutation';
  login: Token;
};


export type MutationLoginArgs = {
  input: Login;
};

export type Query = {
  __typename?: 'Query';
  avgEUI?: Maybe<Array<AvgEui>>;
  buildings?: Maybe<Buildings>;
};


export type QueryBuildingsArgs = {
  keyword: Scalars['String'];
  page: Scalars['Int'];
  pageSize: Scalars['Int'];
};

export type Token = {
  __typename?: 'Token';
  expiredAt: Scalars['Timestamp'];
  value: Scalars['String'];
};

export type RequestBuildingsQueryVariables = Exact<{
  keyword: Scalars['String'];
  page: Scalars['Int'];
  pageSize: Scalars['Int'];
}>;


export type RequestBuildingsQuery = { __typename?: 'Query', buildings?: { __typename?: 'Buildings', page: number, size: number, total: number, items?: Array<{ __typename?: 'Building', OSEBuildingID: string, DataYear: number, BuildingType: string, PrimaryPropertyType: string, PropertyName: string, Address?: string | null, City?: string | null, State?: string | null, ZipCode?: number | null, TaxParcelIdentificationNumber?: string | null, CouncilDistrictCode: number, Latitude: number, Longitude: number, YearBuilt?: number | null, NumberofBuildings?: number | null, NumberofFloors?: number | null, BuildingGFA: Array<{ __typename?: 'BuildingGFA', PropertyUseType: string, PropertyUseTypeGFA: number }>, EnergyStarRating: Array<{ __typename?: 'EnergyStarRating', YearsEnergyStarCertified: string, EnergyStarScore: number }>, Metrics: Array<{ __typename?: 'Metrics', Metric: string, Value: number, Unit: string }> }> | null } | null };

export type RequestAvgEuiQueryVariables = Exact<{ [key: string]: never; }>;


export type RequestAvgEuiQuery = { __typename?: 'Query', avgEUI?: Array<{ __typename?: 'AvgEUI', PrimaryPropertyType: string, EUI: number }> | null };

export type RequestLoginMutationVariables = Exact<{
  username: Scalars['String'];
  password: Scalars['String'];
}>;


export type RequestLoginMutation = { __typename?: 'Mutation', login: { __typename?: 'Token', value: string, expiredAt: any } };


export const RequestBuildingsDocument = /*#__PURE__*/ `
    query RequestBuildings($keyword: String!, $page: Int!, $pageSize: Int!) {
  buildings(keyword: $keyword, page: $page, pageSize: $pageSize) {
    items {
      OSEBuildingID
      DataYear
      BuildingType
      PrimaryPropertyType
      PropertyName
      Address
      City
      State
      ZipCode
      TaxParcelIdentificationNumber
      CouncilDistrictCode
      Latitude
      Longitude
      YearBuilt
      NumberofBuildings
      NumberofFloors
      BuildingGFA {
        PropertyUseType
        PropertyUseTypeGFA
      }
      EnergyStarRating {
        YearsEnergyStarCertified
        EnergyStarScore
      }
      Metrics {
        Metric
        Value
        Unit
      }
    }
    page
    size
    total
  }
}
    `;
export const useRequestBuildingsQuery = <
      TData = RequestBuildingsQuery,
      TError = unknown
    >(
      variables: RequestBuildingsQueryVariables,
      options?: UseQueryOptions<RequestBuildingsQuery, TError, TData>
    ) =>
    useQuery<RequestBuildingsQuery, TError, TData>(
      ['RequestBuildings', variables],
      fetchData<RequestBuildingsQuery, RequestBuildingsQueryVariables>(RequestBuildingsDocument, variables),
      options
    );

useRequestBuildingsQuery.getKey = (variables: RequestBuildingsQueryVariables) => ['RequestBuildings', variables];
;

useRequestBuildingsQuery.fetcher = (variables: RequestBuildingsQueryVariables, options?: RequestInit['headers']) => fetchData<RequestBuildingsQuery, RequestBuildingsQueryVariables>(RequestBuildingsDocument, variables, options);
export const RequestAvgEuiDocument = /*#__PURE__*/ `
    query RequestAvgEUI {
  avgEUI {
    PrimaryPropertyType
    EUI
  }
}
    `;
export const useRequestAvgEuiQuery = <
      TData = RequestAvgEuiQuery,
      TError = unknown
    >(
      variables?: RequestAvgEuiQueryVariables,
      options?: UseQueryOptions<RequestAvgEuiQuery, TError, TData>
    ) =>
    useQuery<RequestAvgEuiQuery, TError, TData>(
      variables === undefined ? ['RequestAvgEUI'] : ['RequestAvgEUI', variables],
      fetchData<RequestAvgEuiQuery, RequestAvgEuiQueryVariables>(RequestAvgEuiDocument, variables),
      options
    );

useRequestAvgEuiQuery.getKey = (variables?: RequestAvgEuiQueryVariables) => variables === undefined ? ['RequestAvgEUI'] : ['RequestAvgEUI', variables];
;

useRequestAvgEuiQuery.fetcher = (variables?: RequestAvgEuiQueryVariables, options?: RequestInit['headers']) => fetchData<RequestAvgEuiQuery, RequestAvgEuiQueryVariables>(RequestAvgEuiDocument, variables, options);
export const RequestLoginDocument = /*#__PURE__*/ `
    mutation RequestLogin($username: String!, $password: String!) {
  login(input: {username: $username, password: $password}) {
    value
    expiredAt
  }
}
    `;
export const useRequestLoginMutation = <
      TError = unknown,
      TContext = unknown
    >(options?: UseMutationOptions<RequestLoginMutation, TError, RequestLoginMutationVariables, TContext>) =>
    useMutation<RequestLoginMutation, TError, RequestLoginMutationVariables, TContext>(
      ['RequestLogin'],
      (variables?: RequestLoginMutationVariables) => fetchData<RequestLoginMutation, RequestLoginMutationVariables>(RequestLoginDocument, variables)(),
      options
    );
useRequestLoginMutation.fetcher = (variables: RequestLoginMutationVariables, options?: RequestInit['headers']) => fetchData<RequestLoginMutation, RequestLoginMutationVariables>(RequestLoginDocument, variables, options);