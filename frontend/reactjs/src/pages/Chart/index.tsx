import { useState, useEffect } from 'react';
import { QueryClient } from '@tanstack/react-query';
import { styled } from '@linaria/react';
import { Chart as ChartJS, CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend } from 'chart.js';
import { Bar } from 'react-chartjs-2';
import { Spin, message } from 'antd';

import { AvgEui, useRequestAvgEuiQuery } from '~/gql/generated/graphql';
import withApplication from '~/App';
import Page from '@components/Page';

const Chart = () => {
	const [isLoading, setIsLoading] = useState(true);
	const [items, setItems] = useState<AvgEui[]>([]);

	ChartJS.register(CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend);

	useEffect(() => {
		const onFetch = async () => {
			try {
				setIsLoading(true);
				const queryClient = new QueryClient();
				const { avgEUI } = await queryClient.fetchQuery(useRequestAvgEuiQuery.getKey(), useRequestAvgEuiQuery.fetcher());
				setItems(avgEUI || []);
			} catch (e: any) {
        message.error(e.message);
			} finally {
				setIsLoading(false);
			}
		};

		onFetch();
	}, []);

	const options = {
		responsive: true,
		plugins: {
			legend: {
				position: 'top' as const,
			},
			title: {
				display: true,
				text: 'Average EUI by Primary Property Type',
			},
		},
	};

	const labels = items?.map((el) => el?.PrimaryPropertyType);

	const data = {
		labels,
		datasets: [
			{
				label: 'Average EUI',
				data: items?.map((el) => el?.EUI),
				backgroundColor: 'dodgerblue',
			},
		],
	};

	return (
		<Page>
			<Container>
				{isLoading ? (
					<Spin tip="Loading..." />
				) : (
					<Bar options={options} data={data} />
				)}
			</Container>
		</Page>
	);
};

const Container = styled.div`
	height: 50vh;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export default withApplication(Chart);
