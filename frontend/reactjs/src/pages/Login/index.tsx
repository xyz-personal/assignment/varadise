import { css } from '@linaria/core';
import { styled } from '@linaria/react';
import { useNavigate } from 'react-router-dom';
import { Card, Form, Input, Button, message } from 'antd';
import { useForm, useWatch, Controller } from 'react-hook-form';

import withApplication from '~/App';
import useAuth from '@contexts/auth';
import { useRequestLoginMutation, RequestLoginMutationVariables } from '@gql/generated/graphql';

interface FormFields {
	username?: string;
	password?: string;
}

const Login = () => {
	const {
		control,
		reset,
		formState: { isSubmitting },
	} = useForm<FormFields>({ defaultValues: { username: '', password: '' } });
	const input = useWatch({ control });

	const navigate = useNavigate();
	const [, { logon }] = useAuth();
	const { isLoading, mutateAsync: requestLogin } = useRequestLoginMutation();

	const onReset = () => {
		reset();
	};

	const onSubmit = async () => {
		try {
			const { login: data } = await requestLogin(input as RequestLoginMutationVariables);
			logon(data);
			message.info('Login Successful');
			navigate('/', { replace: true });
		} catch (e: any) {
			console.log('Login Error', e);
			message.error('Wrong username or password');
		}
	};

	return (
		<Container>
			<Box>
				<Greeting className={cardCss}>
					<Wave />
					<Wave />
					<Wave />
				</Greeting>
				<LoginForm className={cardCss}>
					<Form layout="vertical" autoComplete="off">
						<Form.Item label="Username">
							<Controller
								name="username"
								control={control}
								render={({ field: { name, ...rest } }) => (
									<Input id={name} name={name} disabled={isSubmitting || isLoading} placeholder="admin" {...rest} />
								)}
							/>
						</Form.Item>
						<Form.Item label="Password">
							<Controller
								name="password"
								control={control}
								render={({ field: { name, ...rest } }) => (
									<Input id={name} name={name} type="password" disabled={isSubmitting || isLoading} placeholder="password" {...rest} />
								)}
							/>
						</Form.Item>
						<Form.Item>
							<Button block disabled={isSubmitting || isLoading} onClick={onReset}>
								Reset
							</Button>
						</Form.Item>
						<Form.Item>
							<Button
								block
								disabled={isSubmitting || (!input.username && !input?.password)}
								loading={isLoading}
								type="primary"
								onClick={onSubmit}
							>
								Submit
							</Button>
						</Form.Item>
					</Form>
				</LoginForm>
			</Box>
		</Container>
	);
};

const Greeting = styled(Card)`
	display: flex;
	flex-direction: row;
	border-width: 0px;

	& > * {
		position: relative;
		flex: 1;
	}
`;

const Wave = styled.div`
	animation: growAndFade 3s infinite ease-out;
	background-color: dodgerblue;
	border-radius: 50%;
	opacity: 0;
	position: absolute;
  height: 100%
  width: 100%;
  top: 0;
  left: 0;

  &:nth-child(1) {
    animation-delay: 0s;
  }

  &:nth-child(2) {
    animation-delay: 1s;
  }

  &:nth-child(3) {
    animation-delay: 2s;
  }

	@keyframes growAndFade {
		0% {
			opacity: 0.25;
			transform: scale(0);
		}
		100% {
			opacity: 0;
			transform: scale(1);
		}
	}
`;

const LoginForm = styled(Card)`
	transform: rotateY(180deg);
	background: #ffffff !important;
	box-shadow: 10px 10px 5px 0px rgba(0, 0, 0, 0.2);

	& form {
		width: 100%;
	}
`;

const Box = styled.div`
	width: 320px;
	height: 320px;
	position: relative;
	transition: 0.6s;
	transform-style: preserve-3d;
`;

const Container = styled.div`
	perspective: 1000;

	&:hover ${Box} {
		transform: rotateY(180deg);
	}
`;

const cardCss = css`
	font-size: 1rem;
	width: 320px;
	height: 320px;
	position: absolute;
	z-index: 2;
	text-align: center;
	background: transparent;
	backface-visibility: hidden;
`;

export default withApplication(Login);
