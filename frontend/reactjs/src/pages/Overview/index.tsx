import { useState, useEffect } from 'react';
import { QueryClient } from '@tanstack/react-query';
import { styled } from '@linaria/react';
import { Table, message } from 'antd';

import { Building, useRequestBuildingsQuery } from '~/gql/generated/graphql';
import withApplication from '~/App';
import Page from '@components/Page';

const Overview = () => {
	const [isLoading, setIsLoading] = useState(true);

	const [items, setItems] = useState<Building[]>([]);
	const [total, setTotal] = useState(0);

	const [page, setPage] = useState(1);
	const [pageSize, setPageSize] = useState(10);

	useEffect(() => {
		const onFetch = async () => {
			try {
				setIsLoading(true);
				const queryClient = new QueryClient();
				const { buildings } = await queryClient.fetchQuery(
					useRequestBuildingsQuery.getKey({ keyword: '', page, pageSize }),
					useRequestBuildingsQuery.fetcher({ keyword: '', page, pageSize })
				);
				setItems((buildings?.items || []) as Building[]);
				setTotal(buildings?.total || 0);
			} catch (e: any) {
        message.error(e.message);
			} finally {
				setIsLoading(false);
			}
		};

		onFetch();
	}, [page, pageSize]);

	return (
		<Page>
			<Container>
				<Table
          rowKey="OSEBuildingID"
					loading={isLoading}
					pagination={{
						total,
						pageSize,
						current: page,
						onChange: (p, s) => {
							setPage(p);
							setPageSize(s);
						},
					}}
					expandable={{
						expandedRowRender: (record) => (
							<p style={{ margin: 0 }}>
								{`PrimaryPropertyType: ${record?.PrimaryPropertyType}`}
								<br />
								{`BuildingType: ${record?.BuildingType}`}
								<br />
								{`YearBuilt: ${record?.YearBuilt}`}
								<br />
								{`DataYear: ${record?.DataYear}`}
								<br />
								{`Address: ${[record?.Address, record?.City, record?.State, record?.ZipCode].filter(Boolean).join(', ')}`}
								<br />
								{`CouncilDistrictCode: ${record?.CouncilDistrictCode}`}
								<br />
								{`NumberofBuildings: ${record?.NumberofBuildings}`}
								<br />
								{`NumberofFloors: ${record?.NumberofFloors}`}
								<br />
								{`TaxParcelIdentificationNumber: ${record?.TaxParcelIdentificationNumber}`}<br />
                {'BuildingGFA:'}<br />
								<ul>
									{record?.BuildingGFA?.map((el: any, i) => (
										<li key={i}>
											{Object.keys(el)
												.map((key) => `${key}: ${el[key]}`)
												.join(', ')}
										</li>
									))}
								</ul>
                {'EnergyStarRating:'}<br />
								<ul>
									{record?.EnergyStarRating?.map((el: any, i) => (
										<li key={i}>
											{Object.keys(el)
												.map((key) => `${key}: ${el[key]}`)
												.join(', ')}
										</li>
									))}
								</ul>
                {'Metrics:'}<br />
								<ul>
									{record?.Metrics?.map((el: any, i) => (
										<li key={i}>
											{Object.keys(el)
												.map((key) => `${key}: ${el[key]}`)
												.join(', ')}
										</li>
									))}
								</ul>
							</p>
						),
					}}
					columns={[
						{ dataIndex: 'OSEBuildingID' },
						{ dataIndex: 'PropertyName' },
						{
							title: 'Action',
							dataIndex: '',
							key: 'x',
							render: (_: undefined, record: any) => (
								<a target="_bank" href={`https://www.google.com/maps/search/?api=1&query=${record?.Latitude},${record?.Longitude}`}>
									View
								</a>
							),
						},
					].map((el, i) => ({ key: el?.dataIndex, ...el, title: el.dataIndex }))}
					dataSource={items}
					scroll={{ y: '45vh' }}
				/>
			</Container>
		</Page>
	);
};

const Container = styled.div`
	display: flex;
	flex-direction: column;
	gap: 1rem;
	overflow-y: auto;

	media(min-width: 768px) {
		flex-direction: row;
	}
`;

export default withApplication(Overview);
