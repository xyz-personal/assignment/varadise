import { createBrowserRouter } from 'react-router-dom';

import LoginPage from '@pages/Login';
import OverviewPage from '@pages/Overview';
import ChartPage from '@pages/Chart';

const router = createBrowserRouter([
	{
		path: '/login',
		element: <LoginPage />,
	},
	{
		path: '/',
		element: <OverviewPage />,
	},
	{
		path: '/chart',
		element: <ChartPage />,
	},
]);

export default router;
