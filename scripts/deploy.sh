#!/bin/bash
GREEN='\033[0;32m'
Orange='\033[0;33m'
Blue='\033[0;34m'
NC='\033[0m' # No Color

echo -e "${GREEN}--- Pull latest version from git repo: ---${Blue}"
git reset --hard origin/main
git checkout main
git pull

echo -e "${GREEN}--- Rebuild the docker image: ---${Blue}"
docker-compose build

echo -e "${GREEN}--- Restart the docker image: ---${Blue}"
docker-compose stop && docker-compose up -d

echo -e "${GREEN}--- Done!--- ${NC}"
