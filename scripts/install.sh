#!/bin/bash
GREEN='\033[0;32m'
Blue='\033[0;34m'
NC='\033[0m'

echo -e "${GREEN}--- Install Required Package: ---${Blue}"
sudo apt update
sudo apt install lsb-release ca-certificates apt-transport-https software-properties-common -y

echo -e "${GREEN}--- Install Dcoker: ---${Blue}"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update
sudo apt install docker-ce
sudo systemctl status docker --no-pager
docker -v

echo -e "${GREEN}--- Install Docker Compose: ---${Blue}"
mkdir -p ~/.docker/cli-plugins/
curl -SL https://github.com/docker/compose/releases/download/v2.5.0/docker-compose-linux-x86_64 -o ~/.docker/cli-plugins/docker-compose
chmod +x ~/.docker/cli-plugins/docker-compose
docker compose version

echo -e "${GREEN}--- Done!--- ${NC}"